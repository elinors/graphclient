export default  {
	name: 'StudentGraph',
	children: [
    {
        _id: '0',
        children: [
            {
                name: 1.463179736705023,
                score: 1.463179736705023,
                type: "exam"
            },
            {
                name: 11.78273309957772,
                score: 11.78273309957772,
                type: "quiz"
            },
            {
                name: 35.8740349954354,
                score: 35.8740349954354,
                type: "homework"
            }
        ],
        name: "aimee Zank"
    },
    {
        _id: '1',
        children: [
            {
                name: 60.06045071030959,
                score: 60.06045071030959,
                type: "exam"
            },
            {
                name: 52.79790691903873,
                score: 52.79790691903873,
                type: "quiz"
            },
            {
                name: 71.76133439165544,
                score: 71.76133439165544,
                type: "homework"
            }
        ],
        name: "Aurelia Menendez"
    },
    {
        _id: '2',
        children: [
            {
                name: 67.03077096065002,
                score: 67.03077096065002,
                type: "exam"
            },
            {
                name: 6.301851677835235,
                score: 6.301851677835235,
                type: "quiz"
            },
            {
                name: 66.28344683278382,
                score: 66.28344683278382,
                type: "homework"
            }
        ],
        name: "Corliss Zuk"
    },
    {
        _id: '3',
        children: [
            {
                name: 71.64343899778332,
                score: 71.64343899778332,
                type: "exam"
            },
            {
                name: 24.80221293650313,
                score: 24.80221293650313,
                type: "quiz"
            },
            {
                name: 42.26147058804812,
                score: 42.26147058804812,
                type: "homework"
            }
        ],
        name: "Bao Ziglar"
    },
    {
        _id: '4',
        children: [
            {
                name: 78.68385091304332,
                score: 78.68385091304332,
                type: "exam"
            },
            {
                name: 90.2963101368042,
                score: 90.2963101368042,
                type: "quiz"
            },
            {
                name: 34.41620148042529,
                score: 34.41620148042529,
                type: "homework"
            }
        ],
        name: "Zachary Langlais"
    },
    {
        _id: '5',
        children: [
            {
                name: 44.87186330181261,
                score: 44.87186330181261,
                type: "exam"
            },
            {
                name: 25.72395114668016,
                score: 25.72395114668016,
                type: "quiz"
            },
            {
                name: 63.42288310628662,
                score: 63.42288310628662,
                type: "homework"
            }
        ],
        name: "Wilburn Spiess"
    },
    {
        _id: '6',
        children: [
            {
                name: 37.32285459166097,
                score: 37.32285459166097,
                type: "exam"
            },
            {
                name: 28.32634976913737,
                score: 28.32634976913737,
                type: "quiz"
            },
            {
                name: 81.57115318686338,
                score: 81.57115318686338,
                type: "homework"
            }
        ],
        name: "Jenette Flanders"
    },
    {
        _id: '7',
        children: [
            {
                name: 90.37826509157176,
                score: 90.37826509157176,
                type: "exam"
            },
            {
                name: 42.48780666956811,
                score: 42.48780666956811,
                type: "quiz"
            },
            {
                name: 96.52986171633331,
                score: 96.52986171633331,
                type: "homework"
            }
        ],
        name: "Salena Olmos"
    },
    {
        _id: '8',
        children: [
            {
                name: 22.13583712862635,
                score: 22.13583712862635,
                type: "exam"
            },
            {
                name: 14.63969941335069,
                score: 14.63969941335069,
                type: "quiz"
            },
            {
                name: 75.94123677556644,
                score: 75.94123677556644,
                type: "homework"
            }
        ],
        name: "Daphne Zheng"
    },
    {
        _id: '9',
        children: [
            {
                name: 97.00509953654694,
                score: 97.00509953654694,
                type: "exam"
            },
            {
                name: 97.80449632538915,
                score: 97.80449632538915,
                type: "quiz"
            },
            {
                name: 25.27368532432955,
                score: 25.27368532432955,
                type: "homework"
            }
        ],
        name: "Sanda Ryba"
    },
    {
        _id: '10',
        children: [
            {
                name: 45.61876862259409,
                score: 45.61876862259409,
                type: "exam"
            },
            {
                name: 98.35723209418343,
                score: 98.35723209418343,
                type: "quiz"
            },
            {
                name: 55.90835657173456,
                score: 55.90835657173456,
                type: "homework"
            }
        ],
        name: "Denisha Cast"
    },
    {
        _id: '11',
        children: [
            {
                name: 78.42617835651868,
                score: 78.42617835651868,
                type: "exam"
            },
            {
                name: 82.58372817930675,
                score: 82.58372817930675,
                type: "quiz"
            },
            {
                name: 87.49924733328717,
                score: 87.49924733328717,
                type: "homework"
            }
        ],
        name: "Marcus Blohm"
    },
    {
        _id: '12',
        children: [
            {
                name: 54.29841278520669,
                score: 54.29841278520669,
                type: "exam"
            },
            {
                name: 85.61270164694737,
                score: 85.61270164694737,
                type: "quiz"
            },
            {
                name: 80.40732356118075,
                score: 80.40732356118075,
                type: "homework"
            }
        ],
        name: "Quincy Danaher"
    },
    {
        _id: '13',
        children: [
            {
                name: 90.47179954427436,
                score: 90.47179954427436,
                type: "exam"
            },
            {
                name: 90.3001402468489,
                score: 90.3001402468489,
                type: "quiz"
            },
            {
                name: 95.17753772405909,
                score: 95.17753772405909,
                type: "homework"
            }
        ],
        name: "Jessika Dagenais"
    },
    {
        _id: '14',
        children: [
            {
                name: 25.15924151998215,
                score: 25.15924151998215,
                type: "exam"
            },
            {
                name: 68.64484047692098,
                score: 68.64484047692098,
                type: "quiz"
            },
            {
                name: 24.68462152686763,
                score: 24.68462152686763,
                type: "homework"
            }
        ],
        name: "Alix Sherrill"
    },
    {
        _id: '15',
        children: [
            {
                name: 69.1565022533158,
                score: 69.1565022533158,
                type: "exam"
            },
            {
                name: 3.311794422000724,
                score: 3.311794422000724,
                type: "quiz"
            },
            {
                name: 45.03178973642521,
                score: 45.03178973642521,
                type: "homework"
            }
        ],
        name: "Tambra Mercure"
    }]
};
