export const getData = async () => {
    
    const response = await fetch('http://127.0.0.1:5000/students',
      {
        method: 'GET'
      });

      console.log('get data - response', response);

      return{
          status: response.status,
          data: await response.json()
      };
};