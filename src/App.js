import React from 'react';
import Tree from 'react-tree-graph';
import './App.css';
import { getData } from './fetch';
import { easeElastic } from 'd3-ease';
import json from './json';

class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      data: {}, 
      dataToShow: {},
    }
  }

  componentDidMount() {
    this.fetch();
    
  }
  fetch = () => {
    getData().then((response) => {
      if(response.status === 200) {
        console.log('response.status ', response.status );
        console.log('fetched the data', response.data);

        this.setState({ data : response.data, isFetchSucceded: true })
        this.dataToShow(response.data);
      }
      else{
        this.setState({ dataToShow: json})
      }
   
    }).catch((ex)=>{
      console.log(ex);
    })
  }

  dataToShow = (data) => {
    const children10 = data.children.filter((key, i) => ( i < 15))

    const objectToShow = {
      name: 'People',
      children: children10
    }

    console.log('objectToShow', objectToShow);

    this.setState({ objectToShow : objectToShow });
  }
  
  onClick= (event, node) => {
        alert(node)
}
  

  render() {
    console.log('data', this.state.data)
    return (
      <div className="App">
        <header className="App-header">
          <p>
         ArcusTeam
          </p>
        </header>
       
    <div className="custom-container">
      <Tree
        data={ json }
        height={ window.innerHeight }
        width={ window.innerWidth - 400 }
        margins={ { bottom : 20, left : 20, right : 150, top : 20}}
        gProps= {{
          className: 'node',
          onClick: this.onClick
        }}
        animated
        duration={1000}
        easing={easeElastic}/>
    </div>
  
      </div>
    );
  }
}  

export default App;


	